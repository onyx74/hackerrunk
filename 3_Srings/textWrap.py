#!/usr/bin/env python3

import textwrap

def wrap(string, max_width):
    return textwrap.fill(string, max_width)

string = input()
max_width = int(input())

result = wrap(string, max_width)
print(result)
