def print_rangoli(size):
    string = 'abcdefghijklmnopqrstuvwxyz'
    for i in range(size-1,-size,-1):
        temp = '-'.join(string[size-1:abs(i):-1]+string[abs(i):size])
        print(temp.center(4*size-3,'-'))

n = int(input())
print_rangoli(n)
