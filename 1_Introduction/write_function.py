#!/usr/bin/env python3

# def is_year(year):
#     year = False

#     if year % 4 == 0:
#         if year % 100 != 0:
#             return False
#     elif year % 400 == 0:
#         return True

def is_leap(year):
    return year % 4 == 0 and year % 100 != 0 or year % 400 == 0
year = int(input())
print(is_leap(year))

# def is_leap(year):    
#     if year%4==0:
#         return True
#     elif year%100==0:
#         return False
#     elif year%400==0:
#         return True


# def is_leap(year):
#     leap = False
    
#     if year %4 == 0:
#         if year %400 == 0:
#             leap = True
#         elif year %100 == 0:
#             leap = False
#         else:
#             leap = True
#     else:
#         leap = False 
#     return leap
