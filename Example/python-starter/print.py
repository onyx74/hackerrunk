#!/usr/bin/env python3

name = 'Vetal'
age = 30
print("My name is", name)

# num2 = int(input("Enter 2 number: "))
# num1 = int(input("Enter 1 number: "))
num1 = float(input("Enter 1 number: "))
num2 = int(input("Enter 2 number: "))

result = num1 + num2
result += 1                 #  add 1
result -= 1                 # minus 1
result *= 1
result /= 1
print("Result is: ", result)
 