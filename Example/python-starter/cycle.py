#!/usr/bin/env python3

i = 0

while i <= 10:
    print(i)
    i += 2

for j in "hello world":
    if j == 'a':
        break
    print(j * 2, end='')
else:
    print("\nLiter A not found")
