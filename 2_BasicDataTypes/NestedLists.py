#!/usr/bin/env python3

# students = [['Harry', 37.21], ['Berry', 37.21], ['Tina', 37.2], ['Akriti', 41], ['Harsh', 39]]

# for i in range(int(input())):
#     name = input()
#     score = float(input())
#     students.append([name, score])

# scoreSort = sorted({s[1] for s in students })
# nameSort = sorted(s[0] for s in students if s[1] == scoreSort[1])

# print('\n'.join(nameSort))

n = int(input())
students = []
for _ in range(n):
    name = input()
    grade = float(input())
    students.append([name, grade])
    
givengrades = sorted({x[1] for x in students})
sts = sorted(x[0] for x in students if x[1]==givengrades[1])
for s in sts:
    print(s)
