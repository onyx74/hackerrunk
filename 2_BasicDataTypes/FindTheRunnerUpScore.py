#!/usr/bin/env python3

# Sample Input
# 5
# 2 3 6 6 5

n = int(input())
arr = map(int, input().split())

list = list(arr)
l = (x for x in list if x != max(list))
print(max(l))
